FROM openjdk:18-slim

RUN useradd --create-home appuser
RUN mkdir -p /home/appuser/sherlock

COPY ./target /home/appuser/sherlock/target
RUN chown appuser /home/appuser/sherlock/target

USER appuser
WORKDIR /home/appuser/sherlock/

CMD ["sh", "-c", "java -jar target/sherlock-1.12-SNAPSHOT-jar-with-dependencies.jar --redis-host $REDIS_HOSTNAME --redis-port $REDIS_PORT --egads-config-filename egads_config.ini"]